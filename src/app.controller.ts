import {
  Body,
  Controller,
  Get,
  Post,
  UploadedFile,
  UploadedFiles,
  UseInterceptors,
} from '@nestjs/common';
import {
  AnyFilesInterceptor,
  FileFieldsInterceptor,
  FileInterceptor,
  FilesInterceptor,
} from '@nestjs/platform-express';
import { AppService } from './app.service';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get()
  getHello(): string {
    return this.appService.getHello();
  }

  //upload one file
  @Post('/upload')
  @UseInterceptors(FileInterceptor('file'))
  uploadFile(@UploadedFile() file: Express.Multer.File) {
    console.log(file);
    return {
      file: file,
    };
  }

  //upload an array of files
  // fieldName: as described above
  // maxCount: optional number defining the maximum number of files to accept
  // options: optional MulterOptions object, as described above
  @Post('/uploads')
  @UseInterceptors(FilesInterceptor('file'))
  uploadFiles(@UploadedFiles() files: Array<Express.Multer.File>) {
    console.log(files);
    return {
      files: files,
    };
  }

  // Multiple files
  @Post('/multiplefiles')
  @UseInterceptors(
    FileFieldsInterceptor([
      { name: 'avatar', maxCount: 1 },
      { name: 'background', maxCount: 1 },
    ]),
  )
  uploadMultipleFiles(
    @UploadedFiles()
    files: {
      avatar?: Express.Multer.File[];
      background?: Express.Multer.File[];
    },
  ) {
    console.log(files);
    return {
      files: files,
    };
  }

  // Any files (any name of)
  @Post('/anyfiles')
  @UseInterceptors(AnyFilesInterceptor())
  uploadAnyFiles(
    @Body() body: any,
    @UploadedFiles() files: Array<Express.Multer.File>,
  ) {
    console.log(files);
    return {
      files: files,
      body: body,
    };
  }
}
